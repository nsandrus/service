package ru.nsandrus.service;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

	private volatile SessionFactory sessionFactory = null;

	public SessionFactory getSessionFactory()  
	  {  
	      if (sessionFactory == null)   
	      {  
	    	  synchronized (this) {
				if (sessionFactory == null) {
			         Configuration configuration = new Configuration().configure();
			         ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
			         sessionFactory = configuration.buildSessionFactory(serviceRegistry);             
				}
			}
	      }  
	      return sessionFactory;  
	  }
}
