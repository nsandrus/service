package ru.nsandrus.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import ru.nsandrus.service.entity.Ensemble;
import ru.nsandrus.service.entity.DomainGf;

public class FakeDomain {

	@SuppressWarnings("unchecked")
	public String init(String name41, EntityManagerFactory emf) {

		EntityManager em = emf.createEntityManager();

		EntityTransaction tx = em.getTransaction();

		List<DomainGf> domains = null;
		tx.begin();
		DomainGf domain41 = null;
		javax.persistence.Query q1 = em.createQuery("SELECT c FROM DomainGf c");
		domains = (List<DomainGf>) q1.getResultList();
		for (DomainGf domain : domains) {
			if (domain.getName().equals(name41)) {
				em.remove(domain);
			}
		}

		tx.commit();

		tx = em.getTransaction();
		tx.begin();
		domain41 = new DomainGf();
		domain41.setName(name41);
		domain41.setIpAddress("192.168.191.63");
		domain41.setLastUpdate(new Date());
//		domain41.getUrls().put("ComponentMonthlyFeeHeartBeatURL","http://ms-glass004:5806/MonthlyFeeHeartBeatURL");
//		domain41.getUrls().put("ComponentMonthlyFeeHeartBeatURL","http://ms-glass004:5806/MonthlyFeeHeartBeatURL");
//		domain41.getUrls().put("ComverseProxyWSRegionUrl","http://ms-rwebtst001:8022/Region.svc");
//		domain41.getUrls().put("CRMOperationsUrl","http://ms-glass004:4455/CRMOperationsUrl");
//		domain41.getUrls().put("DSIntegrationTroubleTicketURL","http://ms-glass004");
//		domain41.getUrls().put("EDIProxyEDIUrl","https://vc-test.esphere.ru/service/wsdl/Beeline.wsdl");
//		domain41.getUrls().put("GetCreditServiceSoapURL","http://ms-glass004:20000/GetCreditService");
//		domain41.getNapiProxyUrls().add("ComponentMonthlyFeeNapiProxyURL = http://ms-glass004:8080/NAPIService/NAPIEjbBean");
//		domain41.getNapiProxyUrls().add("DSIntegrationNAPIProxyURL = http://ms-vp0001-0003:8282/NAPIService/NAPIEjbBean");
//		domain41.getNapiProxyUrls().add("ObsEmulatorNAPIProxyURL = http://ms-glass004:4043/NAPIService/NAPIEjbBean");
//		domain41.getNapiProxyUrls().add("OnlinePaymentsNapiProxyURL = http://ms-pieeai002:8088/mockNAPIPortBinding");
//		domain41.getNapiProxyUrls().add("SubscriberInfoNAPIProxyURL = http://ms-glass004:4043/NAPIService/NAPIEjbBean");
//		domain41.getNapiProxyUrls().add("SubscriberSMNapiProxyUrl = http://ms-vp0001-0003:8282/NAPIService/NAPIEjbBean");

		Ensemble component = new Ensemble();
		component.setName("url ensemble");
		component.setDomainGf(domain41);
		em.persist(component);
		em.persist(domain41);

		tx.commit();
		return domain41.toString();

	}
}
