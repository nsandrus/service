package ru.nsandrus.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Use;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.jws.soap.SOAPBinding.Style;

import javax.jws.WebResult;

import ru.nsandrus.service.entity.Ensemble;
import ru.nsandrus.service.entity.DomainGf;
import ru.nsandrus.service.entity.Url;


@WebService
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public class DomainsInfoService {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa");


	/**
	 * Просто для тестирования, дергаем метод и заполняем базу чем-то
	 * 
	 * @param name
	 * @return
	 */
	@WebMethod
	@WebResult(targetNamespace = "http://service.nsandrus.ru/")
	public String initDomain(@WebParam(name = "nameServer") String name) {
		FakeDomain fdomain = new FakeDomain();
		String result = fdomain.init(name, emf);
		return "Init Domains:\n" + result;
	}

	/**
	 * Add domain in DB, equals for name unique
	 * 
	 * @param domain
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(targetNamespace = "http://service.nsandrus.ru/")
	public String addDomain(@WebParam(name = "domain") DomainGf domain) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		// get all domains from DB
		javax.persistence.Query q1 = em.createQuery("SELECT c FROM DomainGf c");
		List<DomainGf> domains = null;

		// search by name equals
		domains = (List<DomainGf>) q1.getResultList();
		for (DomainGf domainInt : domains) {
			if (domainInt.equals(domain)) {
				// store old urls in new instance
//				domain.setEnsemble(domainInt.getEnsemble());
//				domain.setUrlComponents(domainInt.getUrlComponents());
				em.remove(domainInt);
				break;
			}
		}

//		for (Ensemble component : domain.getEnsemble()) {
//			component.setDomainGf(domain);
//		}

		if (domain.getLastUpdate() == null) {
			domain.setLastUpdate(new Date());
		}
		em.persist(domain);
		tx.commit();
		
		return "add Domains:\n" + domain;
	}

	@WebMethod
	@WebResult(targetNamespace = "http://service.nsandrus.ru/")
	public String getDomainText(@WebParam(name = "nameDomain") String name) {
		DomainGf domain = getDomain(name);
		if (domain == null) {
			return "Not found domain";
		}
		return domain.toString();
	}

	/**
	 * Get Domain instance
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(targetNamespace = "http://service.nsandrus.ru/")
	public DomainGf getDomain(@WebParam(name = "nameDomain") String name) {
		EntityManager em = emf.createEntityManager();
		javax.persistence.Query q1 = em.createQuery("SELECT c FROM DomainGf c");
		List<DomainGf> domains = null;
		domains = (List<DomainGf>) q1.getResultList();
		for (DomainGf domainInt : domains) {
			if (domainInt.getName().equals(name)) {
				
				return domainInt;
			}
		}
		return null;
	}

	/**
	 * Get all domains instance
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(targetNamespace = "http://service.nsandrus.ru/", name = "domain")
	public List<DomainGf> getDomains() {
		EntityManager em = emf.createEntityManager();
		javax.persistence.Query q1 = em.createQuery("SELECT c FROM DomainGf c");
		List<DomainGf> domains = (List<DomainGf>) q1.getResultList();
		return domains;
	}
	
	/**
	 * Add domain in DB, equals for name unique
	 * 
	 * @param domain
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(targetNamespace = "http://service.nsandrus.ru/")
	public String removeDomain(@WebParam(name = "domainName") String domainName) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		javax.persistence.Query q1 = em.createQuery("SELECT c FROM DomainGf c");
		List<DomainGf> domains = null;

		domains = (List<DomainGf>) q1.getResultList();
		for (DomainGf domainInt : domains) {
			if (domainInt.getName().equals(domainName)) {
				em.remove(domainInt);
				tx.commit();
				return "domain remove " + domainName;
			}
		}

		return "domain not found";
	}
	

	
	/**
	 * Add urls (data from xml from file domain)
	 * 
	 * @param domain
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(targetNamespace = "http://service.nsandrus.ru/")
	public String addUrls(@WebParam(name = "domainName") String domain, @WebParam(name = "urls") List<Url> urlMap ) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		javax.persistence.Query q1 = em.createQuery("SELECT c FROM DomainGf c");
		List<DomainGf> domains = null;
		DomainGf foundDomain = null;
		domains = (List<DomainGf>) q1.getResultList();
		for (DomainGf domainInt : domains) {
			if (domainInt.getName().equals(domain)) {
				foundDomain = domainInt;
				break;
			}
		}

		if (foundDomain == null) {
			return "domain not found";
		}
		
		
		for (Url component : foundDomain.getUrlComponents()) {
			em.remove(component);
		}
		
		Set<Url> foo = new HashSet<>(urlMap);
		
		foundDomain.setUrlComponents(foo);
		for (Url component : foundDomain.getUrlComponents()) {
			component.setDomainGf(foundDomain);
		}
		
		
		em.persist(foundDomain);
		tx.commit();
		
		return "add Domains:\n" + domain;
	}

	/**
	 * Add urls (data from xml from file domain)
	 * 
	 * @param domain
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(targetNamespace = "http://service.nsandrus.ru/")
	public String addEnsemble(@WebParam(name = "domainName") String domain, @WebParam(name = "napiUrls") List<Ensemble> urlMap ) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		javax.persistence.Query q1 = em.createQuery("SELECT c FROM DomainGf c");
		List<DomainGf> domains = null;
		DomainGf foundDomain = null;
		domains = (List<DomainGf>) q1.getResultList();
		for (DomainGf domainInt : domains) {
			if (domainInt.getName().equals(domain)) {
				foundDomain = domainInt;
//				em.remove(domainInt);
				break;
			}
		}

		if (foundDomain == null) {
			return "domain not found";
		}
		
		
		for (Ensemble component : foundDomain.getEnsemble()) {
			em.remove(component);
		}
		
		
		Set<Ensemble> foo = new HashSet<>(urlMap);
		
		foundDomain.setEnsemble(foo);
		for (Ensemble component : foundDomain.getEnsemble()) {
			component.setDomainGf(foundDomain);
		}
		
		
		em.persist(foundDomain);
		tx.commit();
		
		return "add Domains:\n" + domain;
	}
	
	
}