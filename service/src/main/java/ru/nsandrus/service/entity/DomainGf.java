package ru.nsandrus.service.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Table(name = "DOMAINS")
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainGf {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlTransient
	private long Id;
	
	private String name;
	private String ipAddress;
	private Date lastUpdate; 
	private String adminUrl;
	private String login;
	private String password;
	private String pathToJbiFile;
	private String pathToDomainFile;
	
	@OneToMany(mappedBy = "domainGf",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Url> urlComponents = new HashSet<Url>();
	
	@OneToMany(mappedBy = "domainGf",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Ensemble> ensemble = new HashSet<Ensemble>();
	
	
//	@OneToMany(mappedBy = "domainGf",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	private Set<Ensemble> components = new HashSet<Ensemble>();

	
	
	public String getAdminUrl() {
		return adminUrl;
	}

	public void setAdminUrl(String adminUrl) {
		this.adminUrl = adminUrl;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPathToJbiFile() {
		return pathToJbiFile;
	}

	public void setPathToJbiFile(String pathToJbiFile) {
		this.pathToJbiFile = pathToJbiFile;
	}

	public String getPathToDomainFile() {
		return pathToDomainFile;
	}

	public void setPathToDomainFile(String pathToDomainFile) {
		this.pathToDomainFile = pathToDomainFile;
	}

	
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	



	

	

	public Set<Url> getUrlComponents() {
		return urlComponents;
	}

	public void setUrlComponents(Set<Url> urlComponents) {
		this.urlComponents = urlComponents;
	}

	
	



	public Set<Ensemble> getEnsemble() {
		return ensemble;
	}

	public void setEnsemble(Set<Ensemble> ensemble) {
		this.ensemble = ensemble;
	}

	@Override
	public String toString() {
		return "DomainGf [Id=" + Id + ", name=" + name + ", ipAddress=" + ipAddress + ", lastUpdate=" + lastUpdate + ", adminUrl=" + adminUrl + ", login=" + login + ", password=" + password
				+ ", pathToJbiFile=" + pathToJbiFile + ", pathToDomainFile=" + pathToDomainFile + ", urlComponents=" + urlComponents + ", ensemble=" + ensemble + "]";
	}

	public boolean equals(Object o) {
		 if (this == o) return true;
		 if (!(o instanceof DomainGf)) return false;
		 final DomainGf domain = (DomainGf) o;
		 if (name == null && domain.name == null) return true;
		 if (name == null && domain.name != null) return false;
		 if (!name.equals(domain.name)) return false;
		 return true;
		 }
	
	
}
