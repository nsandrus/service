package ru.nsandrus.service.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "URLS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Url {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlTransient
	private long Id;
	
	private String name;
	private String url;
	

	@XmlTransient
	@ManyToOne
	private DomainGf domainGf;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
	public DomainGf getDomainGf() {
		return domainGf;
	}
	
	
	public void setDomainGf(DomainGf domainGf) {
		this.domainGf = domainGf;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		return "Url [Id=" + Id + ", name=" + name + ", url=" + url + "]";
	}
	
	
	
	

}
